import "../styles/globals.css";
import { ChakraProvider, CSSReset } from "@chakra-ui/react";
import Head from "next/head";
import { extendTheme } from "@chakra-ui/react";
import ErrorBoundary from "../components/ErrorBoundary/view";
import { useRouter } from "next/router";
import { useEffect } from "react";

function MyApp({ Component, pageProps }) {
  const theme = extendTheme({
    styles: {
      global: (props) => ({
        body: {
          fontFamily: "Poppins, sans-serif",
          backgroundColor: "white",
          color:"dark"
        },
      }),
    },
  });

  const router = useRouter();
  useEffect(() => {
    const handleRouteChange = (url) => {};
    router.events.on("routeChangeComplete", handleRouteChange);
    router.events.on("hashChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
      router.events.off("hashChangeComplete", handleRouteChange);
    };
  }, [router.events]);

  return (
    <ErrorBoundary>
      <ChakraProvider theme={theme}>
        <Head>
          <title>Harun Personal Blog</title>
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, viewport-fit=cover"
          />
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet"
          />
        </Head>
        <CSSReset />
        <Component {...pageProps} />
      </ChakraProvider>
    </ErrorBoundary>
  );
}

export default MyApp;
