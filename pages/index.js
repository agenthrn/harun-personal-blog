import { Divider, Flex, Center, Box } from "@chakra-ui/react";
import WorkLists from "../components/Article/WorkLists";
import Contact from "../components/Contact/Contact";
import NavBar from "../components/Navbar/NavBar";
import WelcomeBanner from "../components/WelcomeBanner/Banner";

export default function Home() {
  return (
    <Flex direction="column">
      <NavBar pos="absolute" />
      <WelcomeBanner />
      <Center>
        <WorkLists />
      </Center>
      <Center>
        <Contact />
      </Center>
    </Flex>
  );
}
