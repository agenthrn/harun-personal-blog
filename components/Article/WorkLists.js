import React, { useState, useEffect } from "react";
import {
  Center,
  Text,
  Flex,
  Image,
  Box,
  HStack,
  Link,
  VStack,
  SimpleGrid,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  useDisclosure,
  Code,
  Stack,
} from "@chakra-ui/react";
import { FaRegNewspaper, FaArrowRight } from "react-icons/fa";

export default function WorkLists() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [modalData, setModalData] = useState({});

  const handleClick = (title, image, description, to, techStack, jobDesc) => {
    onOpen();
    setModalData({
      title: title,
      image: image,
      description: description,
      to: to,
      techStack: techStack,
      jobDesc: jobDesc,
    });
  };

  const CardArticle = ({
    title,
    image,
    description,
    to,
    techStack,
    jobDesc,
  }) => {
    return (
      <Box
        onClick={() =>
          handleClick(title, image, description, to, techStack, jobDesc)
        }
        boxShadow="md"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
      >
        <Image objectFit="cover" src={image} />
        <Flex m={4} direction="column">
          <Text fontWeight="600" fontSize={[14, 14, 18, 20]}>
            {title}
          </Text>
          <Text fontWeight="300" fontSize={[12, 12, 14, 14]}>
            {description}
          </Text>
        </Flex>
      </Box>
    );
  };

  const data = [
    {
      title: "Ngegame",
      description: "Apps for create & join e-sport tournaments",
      techStack: ["Nuxt JS", "Node JS", "Lumen", "MySQL", "nginx", "socket.io"],
      to: "https://ngegame.net",
      image: "/ngegame.png",
      jobDesc:
        "Monitoring & enhance front end code, design & create database using MySQL, design & create API using lumen, configure server with nginx & others stuff and build realtime in app chat using socket.io",
    },
    {
      title: "Buildtint",
      description: "Online Wedding Invitation",
      techStack: ["Lumen", "Mongo DB"],
      to: "https://buildtint.com",
      image: "/buildtint.png",
      jobDesc:
        "Design & create database using Mongo DB and design & create API using lumen",
    },
    {
      title: "Poke App",
      description: "Pokemon Catching App",
      techStack: ["React JS"],
      to: "https://poke-app.harunhsb.com/",
      image: "/pokeapp.png",
      jobDesc: "Create Front End using React JS",
    },
  ];

  return (
    <>
      <Modal
        isCentered
        motionPreset="slideInBottom"
        isOpen={isOpen}
        onClose={onClose}
        size="xl"
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{modalData.title}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Image objectFit="cover" src={modalData.image} />
            <Text my={2}>{modalData.description}</Text>
            <Text my={2}>
              <b>Job desc : </b>
              {modalData.jobDesc}
            </Text>
            {modalData.techStack?.map((item, index) => {
              return (
                <Code mr={2} colorScheme="yellow" key={index}>
                  {item}
                </Code>
              );
            })}
          </ModalBody>
          <ModalFooter>
            <Button
              to="{modalData.to}"
              onClick={() => window.open(modalData.to, "_blank")}
              rightIcon={<FaArrowRight />}
            >
              Visit
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Flex maxW={900} px="6" direction="column">
        <HStack spacing="24px" mt="6">
          <FaRegNewspaper color="#F56565" fontSize={28} />
          <Stack>
            <Text fontSize={28} fontWeight={400}>
              Latest works
            </Text>
            <Text mt="0" fontSize={14} fontWeight={300}>
              Click for more info
            </Text>
          </Stack>
        </HStack>
        <SimpleGrid mt="6" columns={[1, 2, 2, 3]} spacing="20px">
          {data.map((item, index) => {
            return (
              <CardArticle
                key={index}
                title={item.title}
                description={item.description}
                to={item.to}
                image={item.image}
                techStack={item.techStack}
                jobDesc={item.jobDesc}
              />
            );
          })}
        </SimpleGrid>
      </Flex>
    </>
  );
}
