import React, { useState, useEffect } from "react";
import {
  Center,
  Text,
  Flex,
  Image,
  Box,
  HStack,
  List,
  ListItem,
  ListIcon,
} from "@chakra-ui/react";
import { MdOutlineContactless, MdOutlineAlternateEmail } from "react-icons/md";
import { FaLinkedin } from "react-icons/fa";

export default function Contact() {
  return (
    <Box id="contact" mb="10" minW={["0", "0", "0", "900"]} maxW={900} px="6">
      <Flex alignItems="center">
        <Box>
          <HStack spacing='24px' my="6">
            <MdOutlineContactless color="#F56565" fontSize={28} />
            <Text fontSize={28} fontWeight={400}>
              Get in touch!
            </Text>
          </HStack>
          <List spacing={3}>
            <ListItem>
              <ListIcon as={MdOutlineAlternateEmail} color="green.500" />
              harunhasibuan5(at)gmail(dot)com
            </ListItem>
            <ListItem>
              <ListIcon as={FaLinkedin} color="blue.500" />
              https://www.linkedin.com/in/harun-arrasyid-hasibuan-38885a134/
            </ListItem>
          </List>
        </Box>
        <Image
          display={["none", "block", "block", "block"]}
          boxSize={["0px", "150px", "250px", "300px"]}
          borderRadius="full"
          objectFit="cover"
          src="/contact.png"
          alt="Harun"
        />
      </Flex>
    </Box>
  );
}
