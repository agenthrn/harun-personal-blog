import {
  Box,
  Button,
  Center,
  ChakraProvider,
  Flex,
  Link,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import React from "react";
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);

    // Define a state variable to track whether is an error or not
    this.state = { hasError: false };
  }
  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI

    return { hasError: true };
  }
  componentDidCatch(error, errorInfo) {
    // You can use your own error logging service here
    console.log({ error, errorInfo });
  }
  render() {
    // Check if the error is thrown
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <div>
          <ChakraProvider>
            <Center minH="100vh">
              <Flex direction="column" alignItems="center">
                <Text fontWeight="bold" mb="8px" fontSize="18px">
                  Oops, terdapat error!
                </Text>
                <Text mb="16px" fontSize="14px">
                  Anda dapat kembali ke home, atau coba lagi
                </Text>
                <SimpleGrid columns={2} spacing="16px">
                  <Link href="/">
                    <Button>Kembali ke Home</Button>
                  </Link>
                  <Button
                    backgroundColor="green"
                    color="white"
                    onClick={() => window.location.reload()}
                  >
                    Coba lagi
                  </Button>
                </SimpleGrid>
              </Flex>
            </Center>
          </ChakraProvider>
        </div>
      );
    }

    // Return children components in case of no error

    return this.props.children;
  }
}

export default ErrorBoundary;
