import React, { useState, useEffect } from "react";
import { Center, Text, Flex, Image, Box } from "@chakra-ui/react";
import styles from "../../styles/WelcomeBanner.module.css";

export default function WelcomeBanner() {
  const [welcome, setWelcome] = useState("Halo");

  let counter = 0;
  const welcomeText = [
    "Guten Tag",
    "Konnichiwa",
    "Merhaba",
    "Annyeong haseyo",
    "Halo",
    "Olá",
    "God dag",
    "Hallo",
  ];

  useEffect(() => {
    setInterval(() => {
      setWelcome(welcomeText[counter++]);
      if (counter >= welcomeText.length) {
        counter = 0;
      }
    }, 4000);
  }, []);

  return (
    <Center minH={500} className={styles.welcome_box}>
      <Flex>
        <Flex padding={10} maxW={600} direction="column">
          <Text
            className={styles.lineUp}
            color="whiteAlpha.800"
            fontSize={40}
            fontWeight={600}
          >
            {` ${welcome} !`}
          </Text>
          <Text color="whiteAlpha.800" fontSize={24} fontWeight={400}>
            <b>Harun Hasibuan</b> aka <b>Harun</b> From Indonesia
          </Text>
          <Text pt="15px" color="whiteAlpha.800" fontSize={16} fontWeight={300}>
            I'm software developer focused on web development (Front End & Back
            End)
          </Text>
        </Flex>
        <Image
          display={["none", "none", "none", "block"]}
          boxSize="250px"
          borderRadius="full"
          objectFit="cover"
          src="/harun_white_bg.png"
          alt="Harun"
        />
      </Flex>
    </Center>
  );
}
