import { Link, Text, Stack, Box } from "@chakra-ui/react";

export default function MenuLinks({ isOpen }) {
  const MenuItem = ({ children, isLast, to = "/", ...rest }) => {
    return (
      <Link href={to}>
        <Text display="block" {...rest}>
          {children}
        </Text>
      </Link>
    );
  };

  return (
    <Box
      bg={["white", "white", "transparent", "transparent"]}
      color={["black", "black", "white", "white"]}
      display={{ base: isOpen ? "block" : "none", md: "block" }}
      flexBasis={{ base: "100%", md: "auto" }}
    >
      <Stack
        spacing={8}
        align="center"
        justify={["center", "space-between", "flex-end", "flex-end"]}
        direction={["column", "row", "row", "row"]}
        padding={5}
      >
        {/* <MenuItem to="/">Home</MenuItem> */}
        <MenuItem to="#contact">Contact</MenuItem>
      </Stack>
    </Box>
  );
}
