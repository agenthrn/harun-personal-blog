import React from "react";
import { Flex, Text, Center, Divider } from "@chakra-ui/react";

export default function Logo() {
  return (
    <Flex minWidth="max-content" alignItems="center" gap="2">
      <Text fontSize="lg" fontWeight="bold">
        Harun
      </Text>
      <Center height="50px">
        <Divider orientation="vertical" />
      </Center>
      <Text fontSize={14} fontWeight="light">
        Palugada Developer
      </Text>
    </Flex>
  );
}
